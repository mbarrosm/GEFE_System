##=====================================================================================================##
########################## Synopsys Design Constraints (SDC) file information ###########################
##=====================================================================================================##
##                                                                                         
## Company: CERN (BE-BI) 
##                                                        
## File Name: Gefe_BaseProject.sdc  
##
## File versions history:
##
##       DATE          VERSION      AUTHOR             DESCRIPTION
##     - 23/11/2016    2.0          M. Barros Marin    Only system timing constraints
##     - 01/11/2016    1.6          M. Barros Marin    Cosmetic modifications
##     - 03/03/2016    1.5          M. Barros Marin    Cosmetic modifications
##     - 29/10/2015    1.0          M. Barros Marin    First .pdc file definition
##
## Libero version: v11.7 11.7.0.119                                                              
##        
## Input Netlist Format: edif
##                                                                                           
## Targeted device:
##
##     - Vendor: Microsemi 
##     - Model:  ProASIC3E(A3PE3000)/ProASIC3L(A3PE3000L) - 896 FBGA
##
## Description:
##
##     System SDC file for the GBT-based Expandable Front-End (GEFE),
##     the standard rad-hard digital board for CERN BE-BI applications.                                                                                                   
##
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!                                                                                                  !!
## !!                             Do not modify the content of this file                               !!
## !!                                                                                                  !!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
##
##=====================================================================================================##
#########################################################################################################
##=====================================================================================================##

# Clocks

create_clock -name {gefe_top|Osc25Mhz_ik}                      -period 40.000000 -waveform {0.000000 20.000000} Osc25Mhz_ik
create_clock -name {gefe_top|i_GefeSystem|GbtxElinksDclkCg_ok} -period 25.000000 -waveform {0.000000 12.500000} GbtxElinksDclkCg_ok

set_clock_groups -asynchronous -group [get_clocks {gefe_top|Osc25Mhz_ik}]
set_clock_groups -asynchronous -group [get_clocks {gefe_top|GbtxElinksDclkCg_ok}]

# False Paths Between Clocks

# False Path Constraints

# Maximum Delay Constraints

# Multicycle Constraints

# Virtual Clocks

# Output Load Constraints

# Driving Cell Constraints

# Wire Loads

# set_wire_load_mode top

# Other Constraints
